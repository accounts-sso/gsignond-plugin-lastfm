Last.FM Plugin for the gSignOn daemon
===========================================

This plugin for the Accounts-SSO gSignOn daemon handles the Last.FM credentials.


Build instructions
------------------

This project depends on GSignond, and uses the Meson build system. To build it, run
```
meson build --prefix=/usr
cd build
ninja
sudo ninja install
```

License
-------

See COPYING.LIB file.

Resources
---------

[Last.FM Plugin documentation](http://accounts-sso.gitlab.io/gsignond-plugin-lastfm/index.html)

[Official source code repository](https://gitlab.com/accounts-sso/gsignond-plugin-lastfm)
